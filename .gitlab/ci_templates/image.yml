---
include:
  - project: cki-project/cki-lib
    file: .gitlab/ci_templates/cki-common.yml

.publish_local:
  extends: .publish_job
  script:
    - |
      # bootstrap a buildah-image like environment from the upstream image
      if [[ "${BOOTSTRAP:-}" = [Tt]rue ]] ; then
        dnf -y --setopt=install_weak_deps=False install \
            cpp \
            lolcat \
            qemu-user-static \
            skopeo \
            toilet \
            util-linux
        curl \
          --config files/curlrc \
          --output /usr/local/bin/qemu-binfmt-conf.sh \
          https://raw.githubusercontent.com/qemu/qemu/master/scripts/qemu-binfmt-conf.sh
        chmod +x /usr/local/bin/qemu-binfmt-conf.sh
      fi
    - |
      # set env variables to correctly handle arch overrides
      if [ -v IMAGE_ARCH ] || [ -v AUTOMATIC_TAGS ]; then
        unset IMAGE_ARCHES
      elif ! [ -v IMAGE_ARCHES ]; then
        export IMAGE_ARCHES="amd64 arm64 ppc64le s390x"
      fi
    - |
    - ./cki_build_image.sh
  interruptible: true

build-amd64-public:
  extends: .publish_local
  variables:
    IMAGE_ARCH: amd64
  rules:
    - if: $REGISTRIES !~ /CI_REGISTRY/
      when: never
    - if: $IMAGE_ARCHES == null || $IMAGE_ARCHES =~ /amd64/

build-amd64-internal:
  extends: .publish_local
  variables:
    IMAGE_ARCH: amd64
  tags:
    - internal-image-build-runner-x86_64
  rules:
    - if: $REGISTRIES =~ /CI_REGISTRY/
      when: never
    - if: $IMAGE_ARCHES == null || $IMAGE_ARCHES =~ /amd64/

build-arm64-public:
  extends: .publish_local
  variables:
    IMAGE_ARCH: arm64
  tags:
    - public-image-build-runner-aarch64
  rules:
    - if: $REGISTRIES !~ /CI_REGISTRY/
      when: never
    - if: $IMAGE_ARCHES == null || $IMAGE_ARCHES =~ /arm64/

build-arm64-internal:
  extends: .publish_local
  variables:
    IMAGE_ARCH: arm64
  tags:
    - internal-image-build-runner-aarch64
  rules:
    - if: $REGISTRIES =~ /CI_REGISTRY/
      when: never
    - if: $IMAGE_ARCHES == null || $IMAGE_ARCHES =~ /arm64/

build-ppc64le-public:
  extends: .publish_local
  variables:
    IMAGE_ARCH: ppc64le
  rules:
    - if: $REGISTRIES !~ /CI_REGISTRY/
      when: never
    - if: $IMAGE_ARCHES == null || $IMAGE_ARCHES =~ /ppc64le/

build-ppc64le-internal:
  extends: .publish_local
  variables:
    IMAGE_ARCH: ppc64le
  tags:
    - internal-image-build-runner-ppc64le
  rules:
    - if: $REGISTRIES =~ /CI_REGISTRY/
      when: never
    - if: $IMAGE_ARCHES == null || $IMAGE_ARCHES =~ /ppc64le/


build-s390x-public:
  extends: .publish_local
  variables:
    IMAGE_ARCH: s390x
  rules:
    - if: $REGISTRIES !~ /CI_REGISTRY/
      when: never
    - if: $IMAGE_ARCHES == null || $IMAGE_ARCHES =~ /s390x/

build-s390x-internal:
  extends: .publish_local
  variables:
    IMAGE_ARCH: s390x
  tags:
    - internal-image-build-runner-s390x
  rules:
    - if: $REGISTRIES =~ /CI_REGISTRY/
      when: never
    - if: $IMAGE_ARCHES == null || $IMAGE_ARCHES =~ /s390x/

.build-multi:
  extends: .publish_local
  stage: 🎁

build-multi-gitlab:
  extends: .build-multi
  variables:
    REGISTRY: CI_REGISTRY
  rules:
    - if: $REGISTRIES =~ /CI_REGISTRY/
      when: on_success

build-multi-quay:
  extends: .build-multi
  variables:
    REGISTRY: QUAY_IO
  rules:
    - if: $REGISTRIES =~ /QUAY_IO/
      when: on_success

test-pipeline-definition:
  stage: 💉
  trigger:
    project: cki-project/pipeline-definition
    strategy: depend
  variables:
    ONLY_IMAGE: $IMAGE_NAME
    image_tag: p-$PARENT_PIPELINE_ID
    latest_image_tag: p-$PARENT_PIPELINE_ID
  rules:
    - if: $SKIP_PIPELINE_TESTS == null

test-tuxmake:
  image: quay.io/cki/${IMAGE_NAME}:p-${PARENT_PIPELINE_ID}
  stage: 💉
  script:
    - |
      # clang and gcc
      for toolchain in gcc clang; do
        # pass empty string to --target for x86_64
        for arch in "" aarch64 powerpc s390; do
          tuxmake --check-environment --toolchain "$toolchain" --target "$arch"
        done
      done

      # llvm
      # no --target for x86_64
      tuxmake --check-environment --toolchain llvm
      tuxmake --check-environment --toolchain llvm --target aarch64
  rules:
    - if: '$TEST_TUXMAKE =~ /^[Tt]rue$/'

test-buildah:
  extends: .publish
  image: quay.io/cki/${IMAGE_NAME}:p-${PARENT_PIPELINE_ID}
  stage: 💉
  variables:
    IMAGE_ARCH: amd64
    REGISTRY: 'dummy'          # hack: disable REGISTRIES from parent pipeline
  before_script:
    - rm -r includes           # force using the includes from the image
    - export IMAGE_NAME=smoke  # setting it in variables: interacts with rules:
  rules:
    - if: $IMAGE_NAME == 'buildah'

tag:
  extends: .publish_local
  stage: 📦
  variables:
    AUTOMATIC_TAGS: 'true'
