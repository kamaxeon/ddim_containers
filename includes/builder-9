/* RHEL or CentOS Stream? */
#if defined _RHEL_VERSION
#include "setup-from-rhel"
#else
#include "setup-from-stream"
#endif

RUN echo "fastestmirror=true" >> /etc/dnf/dnf.conf

/* HACK: some packages in both c9s and RHEL9 images are horribly broken */
#if defined _RHEL_VERSION
RUN dnf -y reinstall ncurses-libs
#else
RUN dnf -y reinstall tzdata
#endif

#include "builder-all"

/* The language definitions are separated and we need this package to set en_US.UTF-8 */
RUN dnf -y install glibc-langpack-en

/* Install the required packages for kernel RPM building. */
RUN dnf -y install \
    annobin \
    bpftool \
    clang \
    dwarves \
    fuse-devel \
    gcc-c++ \
    kabi-dw \
    libbpf-devel \
    libcap-devel \
    libcap-ng-devel \
    libkcapi-hmaccalc \
    libmnl-devel \
    libtraceevent-devel \
    llvm \
    nss-tools \
    openssl-devel \
    perl-generators \
    python3 \
    python3-devel \
    python3-pip \
    python3-sphinx \
    python3-sphinx_rtd_theme

#if defined _RHEL_VERSION
RUN dnf -y install redhat-sb-certs
#else
RUN dnf -y install centos-sb-certs
#endif

#if !defined(_RHEL_VERSION) || !defined(_RHEL_MINOR_VERSION) || (_RHEL_MINOR_VERSION >= 1)
/* Some extra dependencies for kernel RPM building at RHEL 9.1 and newer */
RUN if [ "$(arch)" = "x86_64" ] ; then \
      dnf -y install \
        libnl3-devel ; \
    fi
#endif

RUN if [ "$(arch)" = "x86_64" ] ; then \
      dnf -y install \
        binutils-aarch64-linux-gnu \
        binutils-ppc64le-linux-gnu \
        binutils-s390x-linux-gnu \
        gcc-aarch64-linux-gnu \
        gcc-ppc64le-linux-gnu \
        gcc-s390x-linux-gnu ; \
    fi

/* Set Python 3 as the preferred version. */
RUN ln -sf /usr/bin/python3 /usr/libexec/platform-python

/* Install AWS cli via pip to talk to S3 as there's no RHEL9 package yet. */
RUN python3 -m pip install --ignore-installed awscli

#include "cleanup"
